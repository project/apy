'use strict';

(function (Drupal, once) {
  const onceName = "apy-admin";

  Drupal.behaviors.apyAdmin = {
    attach: function attach(context) {
      const details = document.querySelectorAll('.apy-details');
      once(onceName, context.querySelector('.apy-toggle')).forEach(function (toggle) {
        toggle.addEventListener('change', function () {
          details.forEach(function (detail) {
            if (toggle.checked) {
              detail.setAttribute('open', 'true');
            } else {
              detail.removeAttribute('open')
            }
          });
        });
      });
    },
    detach: function detach(context, settings, trigger) {
      if (trigger === 'unload') {
        once.remove(onceName, context.querySelector('.apy-toggle'));
      }
    }
  };
})(Drupal, once);
