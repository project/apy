<?php

namespace Drupal\apy\Plugin\Apy;

use Drupal\apy\Plugin\Apy\ApyPluginBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * @Apy(
 *   id = "default",
 *   label = @Translation("Default"),
 *   defaults = {
 *     "remove_empty_fields" = FALSE,
 *     "remove_value_target_id" = TRUE,
 *     "flatten_single_value" = TRUE,
 *     "view_mode" = "",
 *   }
 * )
 */
class ApyDefault extends ApyPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $plugin_id = $this->getPluginId();

    $form[$plugin_id]['remove_empty_fields']['#type'] = 'checkbox';
    $form[$plugin_id]['remove_empty_fields']['#title'] = $this->t('Remove empty fields');
    $form[$plugin_id]['remove_empty_fields']['#description'] = $this->t("If checked, fields without value won't be exposed in the JSON output.");

    $form[$plugin_id]['remove_value_target_id']['#type'] = 'checkbox';
    $form[$plugin_id]['remove_value_target_id']['#title'] = $this->t('Remove value/target_id');
    $form[$plugin_id]['remove_value_target_id']['#description'] = $this->t('If checked, <code>$field[0]["value/target_id"]</code> will be removed, exposing only the value <code>$field[0]</code> in the JSON output.');

    $form[$plugin_id]['flatten_single_value']['#type'] = 'checkbox';
    $form[$plugin_id]['flatten_single_value']['#title'] = $this->t('Flatten single value field');
    $form[$plugin_id]['flatten_single_value']['#description'] = $this->t("By default, all fields are exposed as an array. If checked, fields with one value only will be exposed as such - as a string, not an array - in the JSON output.");

    // @todo Retrieve view modes.
    $view_modes = ['default' => 'default'];
    
    $form[$plugin_id]['view_mode']['#type'] = 'select';
    $form[$plugin_id]['View_mode']['#title'] = $this->t('Use fields from Display mode');
    $form[$plugin_id]['view_mode']['#empty_option'] = $this->t('- Select -');
    $form[$plugin_id]['view_mode']['#options'] = $view_modes;

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function normalize(EntityInterface $entity, array $data = []) {
    $data = !empty($data) ? $data : parent::normalize($entity);

    if ($view_mode = $this->configuration['view_mode'] ?? NULL) {
      if ($display = EntityViewDisplay::collectRenderDisplay($entity, $view_mode)) {
        $keys = array_keys($display->getComponents());
        $data = array_filter($data, function ($key) use ($keys) {
          return in_array($key, $keys);
        }, ARRAY_FILTER_USE_KEY);
      }
    }

    foreach ($data as $key => $items) {
      if ($this->configuration['remove_empty_fields'] ?? FALSE) {
        if (empty($items)) {
          unset($data[$key]);
          continue;
        }

        foreach ($items as $delta => $item) {
          if ($this->configuration['remove_value_target_id'] ?? FALSE) {
            $data[$key][$delta] = $item['value'] ?? $item['target_id'] ?? $item;
          }
        }

        if ($this->configuration['flatten_single_value'] ?? FALSE) {
          if (isset($data[$key][0]) && count($items) == 1) {
            $data[$key] = $data[$key][0];
          }
        }
      }
    }

    return $data;
  }
}
