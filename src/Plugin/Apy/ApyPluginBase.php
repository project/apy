<?php

namespace Drupal\apy\Plugin\Apy;

use Drupal\apy\ApyPluginInterface;
use Drupal\apy\PluginForm\ApyPluginForm;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base apy plugin.
 */
class ApyPluginBase extends PluginBase implements ApyPluginInterface, PluginWithFormsInterface, ContainerFactoryPluginInterface {

  /**
   * A config object for this plugin.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * @var mixed|null
   */
  protected $input = NULL;

  /**
   * @var mixed|null
   */
  protected $output = NULL;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $settings = $config_factory->get("apy.plugin.$plugin_id.settings");
    $settings = $settings ? $settings->getRawData() : [];

    // Merge existing settings, defaults from Annotation and current configuration.
    $this->configuration += $settings + $this->getPluginDefinition()['defaults'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
    );
  }


  /**
   * {@inheritDoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormClass($operation) {
    return $this->pluginDefinition['forms'][$operation] ?? ApyPluginForm::class;
  }

  /**
   * {@inheritDoc}
   */
  public function hasFormClass($operation) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $plugin_id = $this->getPluginId();

    $form['#tree'] = TRUE;

    foreach ($this->getConfiguration() as $key => $value) {
      if (strpos($key, '_') === 0) {
        continue;
      }

      $form[$plugin_id][$key] = [
        '#type' => 'textfield',
        '#title' => ucfirst($key),
        '#default_value' => $value,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Nothing specific to validate.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $plugin_id = $this->getPluginId();

    foreach ($form_state->getValues()[$plugin_id] ?? [] as $key => $value) {
      $this->configuration[$key] = Xss::filter($value);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function normalize(EntityInterface $entity) {
    $data = $entity->toArray();
    
    // Access check.
    foreach (array_keys($data) as $field) {
      if (!$entity->hasField($field) || !$entity->get($field)->access()) {
        unset($data[$field]);
      }
    }
    
    return $data;
  }
}
