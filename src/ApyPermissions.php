<?php

namespace Drupal\apy;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions of the APY module.
 */
class ApyPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new FilterPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
    );
  }

  /**
   * Returns an array of filter permissions.
   *
   * @return array
   */
  public function permissions() {
    $permissions = [];

    $permissions['apy view'] = [
      'title' => $this->t('Access APY main endpoint'),
    ];

    $config = $this->configFactory->get('apy.settings');
    $exposed_entity_types = array_filter($this->entityTypeManager->getDefinitions(), function ($entity_type, $id) use ($config) {
      return in_array($id, $config->get('exposed_' . $entity_type->getGroup() . '_entity_types') ?? []);
    }, ARRAY_FILTER_USE_BOTH);

    $operations = ['view', 'update', 'delete'];
    foreach ($operations as $op) {
      foreach ($exposed_entity_types as $entity_type_id => $entity_type) {
        $parts = [];
        $parts[] = 'apy';
        $parts[] = $op;
        $parts[] = $entity_type_id;

        // Entity type permission.
        $permission = implode(' ', array_filter($parts));
        $permissions[$permission] = $this->getPermission($op, $entity_type);

        if ($bundle_entity_type_id = $entity_type->getBundleEntityType()) {
          $bundles = $this->entityTypeManager->getStorage($bundle_entity_type_id)->loadMultiple();
          foreach ($bundles as $bundle_id => $bundle) {
            $subparts = $parts;
            $subparts[] = $bundle_id;

            // Entity type permission.
            $permission = implode(' ', array_filter($subparts));
            $permissions[$permission] = $this->getPermission($op, $entity_type, $bundle);
          }
        }
      }
    }

    return $permissions;
  }

  /**
   * Utility method to generate a permission.
   * 
   * @return array
   */
  protected function getPermission(string $operation, EntityTypeInterface $entity_type, $bundle = NULL) {
    $dependencies = [];
    if ($bundle) {
      $dependencies[$bundle->getConfigDependencyKey()] = [$bundle->getConfigDependencyName()];
    }

    return [
      'title' => $this->t('APY: @operation @entity_type@bundle', [
        '@operation' => $operation,
        '@entity_type' => $entity_type->getPluralLabel(),
        '@bundle' => $bundle ? ':' . $bundle->label() : NULL,
      ]),
      'dependencies' => $dependencies,
    ];
  }
}
