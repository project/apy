<?php

namespace Drupal\apy\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Render\Element\PathElement;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for apy settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The plugin manager.
   *
   * @var \Drupal\apy\ApyPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * The plugin form factory.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  protected $pluginCacheClearer;
  protected $routeBuilder;
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apy_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $configs = ['apy.settings'];

    $plugin_ids = array_keys($this->pluginManager->getDefinitions());
    foreach ($plugin_ids as $plugin_id) {
      $configs[] = "apy.plugin.$plugin_id.settings";
    }

    return $configs;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->pluginManager = $container->get('plugin.manager.apy');
    $instance->pluginFormFactory = $container->get('plugin_form.factory');
    $instance->pluginCacheClearer = $container->get('plugin.cache_clearer');
    $instance->routeBuilder = $container->get('router.builder');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('apy.settings');

    $default_plugin_id = $config->get('default_plugin') ?? 'default';

    $plugins = array_column($this->pluginManager->getDefinitions(), 'label', 'id');
    uksort($plugins, function ($a, $b) use ($default_plugin_id) {
      $a_weight = $a == $default_plugin_id ? 0 : 1;
      $b_weight = $b == $default_plugin_id ? 0 : 1;

      return  $a_weight <=> $b_weight;
    });

    $form['setting'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['global'] = [
      '#type' => 'details',
      '#title' => $this->t('Global'),
      '#group' => 'setting',
    ];

    $form['global']['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint path'),
      '#default_value' => $config->get('endpoint') ?? '/apy',
      '#description' => $this->t('Base path to access the APY.'),
      '#required' => TRUE,
    ];

    $entity_types = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      $entity_types[$entity_type->getGroup()][$entity_type_id] = $entity_type->getPluralLabel();
    }
    uksort($entity_types, function ($a, $b) {
      $a_weight = $a == 'content' ? 0 : 1;
      $b_weight = $b == 'content' ? 0 : 1;

      return  $a_weight <=> $b_weight;
    });


    $form['global']['default_plugin'] = [
      '#type' => 'select',
      '#options' => $plugins,
      '#title' => $this->t('Default plugin'),
      '#default_value' => $default_plugin_id,
      '#required' => TRUE,
    ];
    foreach ($entity_types as $group => $options) {
      $form['global']['exposed_' . $group . '_entity_types'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Exposed @group', ['@group' => $group]),
        '#options' => $options,
        '#default_value' => $config->get('exposed_' . $group . '_entity_types') ?? [],
        '#open' => TRUE,
      ];
    }

    foreach ($plugins as $plugin_id => $plugin_title) {
      $form[$plugin_id . '_wrapper'] = [
        '#type' => 'details',
        '#title' => $this->t('@plugin plugin', ['@plugin' => $plugin_title]),
        '#group' => 'setting',
      ];

      $form[$plugin_id] = ['#parents' => ['plugins']];

      $plugin_settings = $settings['options'][$plugin_id] ?? [];
      if ($plugin_config = $this->configFactory->get('apy.plugin.' . $plugin_id . '.settings')) {
        $plugin_settings += $plugin_config->getRawData();
      }

      $plugin = $this->pluginManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $form[$plugin_id . '_wrapper'][] = $plugin_form->buildConfigurationForm($form[$plugin_id], $subform_state);
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Utility method to get a plugin form.
   */
  public function getPluginForm($plugin, $operation) {
    if ($plugin instanceof PluginWithFormsInterface) {
      if ($plugin->hasFormClass($operation)) {
        return $this->pluginFormFactory->createInstance($plugin, $operation);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $endpoint = $form_state->getValue('endpoint') ?? NULL;
    if (!$endpoint || $endpoint == '/') {
      $form_state->setErrorByName('endpoint', $this->t('Endpoint path not valid.'));
    }

    $plugins = $form_state->getValue('plugins') ?? [];
    unset($plugins['plugins__active_tab']);

    foreach ($plugins as $plugin_id => $plugin_settings) {
      $plugin = $this->pluginManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $plugin_form->validateConfigurationForm($form[$plugin_id], $subform_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('apy.settings');
    $exclude = ['submit', 'form_build_id', 'form_token', 'form_id', 'op', 'actions', 'plugins__active_tab'];
    foreach ($form_state->getValues() as $key => $data) {
      if (!in_array($key, $exclude)) {
        // Clean list of entity types.
        if (strpos($key, 'exposed_') === 0 && \is_array($data)) {
          $data = array_filter($data, function ($value) {
            return $value !== 0;
          });
        }
        $config->set($key, $data);
      }
    }
    $config->save();


    $plugins = $form_state->getValue('plugins') ?? [];
    unset($plugins['plugins__active_tab']);

    foreach ($plugins as $plugin_id => $plugin_settings) {
      $plugin = $this->pluginManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $plugin_form->submitConfigurationForm($form[$plugin_id], $subform_state);

        $plugin_config = $this->config("apy.plugin.$plugin_id.settings");
        foreach ($plugin_settings as $key => $value) {
          $plugin_config->set($key, $value);
        }
        $plugin_config->save();
      }
    }

    // Clear enough caches to enable/disable "Usage" tab for selected entity types.
    $this->pluginCacheClearer->clearCachedDefinitions();
    $this->routeBuilder->rebuild();
    $this->messenger()->addStatus($this->t('Caches cleared.'));

    parent::submitForm($form, $form_state);
  }
}
