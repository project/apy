<?php

namespace Drupal\apy\Controller;

use Drupal\apy\ApyPluginManagerInterface;
use Drupal\commerce\Plugin\Commerce\InlineForm\ContentEntity;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Xss;
use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Tests\Core\Access\AccessResultTest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Route;

/**
 * Returns responses for apy module routes.
 */
class ApyController extends ControllerBase {

  /**
   * The APY plugin manager.
   * 
   * @var \Drupal\apy\ApyPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs an instance of this controller.
   *
   * @param \Drupal\apy\ApyPluginManagerInterface
   *   The APY plugin manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack
   *   The request stack.
   */
  public function __construct(
    ApyPluginManagerInterface $plugin_manager,
    RequestStack $request_stack
  ) {
    $this->pluginManager = $plugin_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * Retrieves entity from route match.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity object as determined from the passed-in route match.
   */
  protected function getEntityFromRouteMatch(RouteMatchInterface $route_match) {
    $parameter_name = $route_match->getRouteObject()->getOption('_apy_entity_type_id');
    return $route_match->getParameter($parameter_name);
  }

  /**
   * Utility method to get settings for a given entity.
   * 
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   A given entity, either config or content entity type.
   * 
   * @return array
   *   The settings.
   */
  public function getSettingsFromEntity(EntityInterface $entity) {
    $settings = [];

    // Global defaults settings.
    if ($config = $this->config('apy.settings')) {
      $settings['plugins'][] = $config->get('default_plugin') ?? 'default';
    }

    // Custom settings per entity type.
    if ($config = $this->config('apy.' . $entity->getEntityTypeId()  . '.settings')) {
      $settings += $config->getRawData() ?? [];
    }

    // Custom settings per bundle.
    if ($bundle_type = $entity->getEntityType()->getBundleEntityType()) {
      $bundle = $this->entityTypeManager()->getStorage($bundle_type)->load($entity->bundle());
      $settings += $bundle->getThirdPartySettings('apy') ?? [];
    }

    return $settings;
  }

  /**
   * Get page title for requested APY route.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A RouteMatch object.
   *
   * @return string
   */
  public function fromApyRouteTitle(RouteMatchInterface $route_match) {
    $entity = $this->getEntityFromRouteMatch($route_match);
    return $entity instanceof EntityInterface ? $entity->label() : 'APY';
  }

  /**
   * Display the admin page a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   * @param array $settings
   *   (optional) A list of settings for APY for this entity's bundle.
   * 
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function fromApyRoute(RouteMatchInterface $route_match) {
    $entity = $this->getEntityFromRouteMatch($route_match);
    if (!$entity instanceof EntityInterface) {
      return ['#markup' => $this->t('Missing entity reference.')];
    }

    $settings = $this->getSettingsFromEntity($entity);

    $selected_plugins = $settings['plugins'] ?? [];
    if (empty($selected_plugins)) {
      return ['#markup' => $this->t('No APY plugin(s) selected.')];
    }

    $build = [];

    $build['toggle'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open/close all'),
      '#attributes' => ['class' => ['apy-toggle']],
    ];

    $data = [];
    foreach ($selected_plugins as $plugin_id) {
      $plugin_settings = $settings['options'][$plugin_id] ?? [];
      if ($plugin = $this->pluginManager->createInstance($plugin_id, $plugin_settings)) {
        $data = $plugin->normalize($entity, $data);

        foreach ($data as $key => $value) {
          $build[$plugin_id][$key] = [
            '#type' => 'details',
            '#title' => $key,
            '#attributes' => ['class' => ['apy-details']],
            'value' => [
              '#type' => 'inline_template',
              '#template' => '<pre><code class="apy-code">{{ dump(value) }}</code></pre>',
              '#context' => [
                'value' => $value,
              ],
            ],
          ];
        }
      }
    }

    $build['#attached']['library'][] = 'apy/admin';

    return $build;
  }

  /**
   * Get page title for APY endpoints.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A RouteMatch object.
   *
   * @return string
   */
  public function endpointTitle(RouteMatchInterface $route_match) {
    return $this->config('system.site')->get('name');
  }

  /**
   * Dispatch request to our custom route to associated methods.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A RouteMatch object.
   *
   * @return array|\Symfony\Component\HttpFoundation\JsonResponse
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   * 
   * @todo Allow requesting configuration entities too.
   */
  public function endpoint(RouteMatchInterface $route_match) {
    $query = $this->requestStack->getCurrentRequest()->query->all();
    foreach ($query as $key => $value) {
      $query[Xss::filter($key)] = Xss::filter($value);
    }

    $route = $route_match->getRouteObject();
    $entity_type = $route->getOption('_apy_entity_type_id') ?? $query['entity_type'] ?? NULL;
    $entity_bundle = $route->getOption('_apy_entity_bundle') ?? $query['entity_bundle'] ?? NULL;
    $uuid = $query['id'] ?? NULL;

    if (!$entity_type) {
      return $this->handleEndpoint();
    }
    if (!$entity_bundle && empty($uuid)) {
      return $this->handleEntityType($entity_type);
    }
    if ($entity_bundle && empty($uuid)) {
      return $this->handleEntityBundle($entity_type, $entity_bundle);
    }
    if (!empty($uuid)) {
      $id_key = Uuid::isValid($uuid) ? 'uuid' : 'id';
      $definition = $this->entityTypeManager()->getDefinition($entity_type);
      $entities = $this->entityTypeManager()->getStorage($entity_type)->loadByProperties([
        $definition->getKey($id_key) => $uuid
      ]);
      $entity = reset($entities) ?? NULL;

      if (!$entity instanceof EntityInterface) {
        return new JsonResponse([
          'error' => $this->t('Entity @type @id not found', [
            '@type' => $entity_type,
            '@id' => $uuid,
          ])
        ]);
      }

      $settings = $this->getSettingsFromEntity($entity);

      $method = $this->requestStack->getCurrentRequest()->getMethod();
      switch ($method) {
        case 'DELETE':
          return $this->handleDelete($entity, $settings);
        case 'PATCH':
          return $this->handlePatch($entity, $settings);
        default:
          return $this->handleGet($entity, $settings);
      }
    }

    return new JsonResponse(['error' => 'Malformatted request.']);
  }

  /**
   * Render the first-level endpoint.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The list of data exposed by APY.
   */
  public function handleEndpoint() {
    $data = [];

    $config = $this->config('apy.settings');

    $exposed_entity_types = array_filter($this->entityTypeManager()->getDefinitions(), function ($entity_type, $id) use ($config) {
      return in_array($id, $config->get('exposed_' . $entity_type->getGroup() . '_entity_types') ?? []);
    }, ARRAY_FILTER_USE_BOTH);

    foreach ($exposed_entity_types as $entity_type_id => $entity_type) {
      if ($entity_type->hasLinkTemplate('apy')) {
        if (($entity_type_url = Url::fromRoute("apy.$entity_type_id"))->access()) {
          $data[$entity_type_id] = $entity_type_url
            ->setAbsolute()
            ->toString();
        }
      }
    }

    return new JsonResponse($data);
  }

  /**
   * Render the entity endpoint.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The list of data exposed by APY.
   */
  public function handleEntityType(string $entity_type_id) {
    $data = [];

    $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);

    $data['label'] = $entity_type->getLabel();

    $bundles = [];
    if ($bundle_entity_type_id = $entity_type->getBundleEntityType()) {
      $bundles = $this->entityTypeManager()->getStorage($bundle_entity_type_id)->loadMultiple();
    }

    // List of bundles.
    foreach (array_keys($bundles) as $bundle_id) {
      if (($bundle_url = Url::fromRoute("apy.$entity_type_id.$bundle_id"))->access()) {
        $data['bundles'][$bundle_id] = $bundle_url
          ->setAbsolute()
          ->toString();
      }
    }


    // @todo Is there a better way to render config entities?
    if ($entity_type instanceof ConfigEntityTypeInterface) {
      foreach ($entity_type->getPropertiesToExport() as $key) {
        $data['properties'][$key] = $entity_type->get($key);
      }
    }

    if ($entity_type instanceof ContentEntityTypeInterface) {
      $data['total'] = Json::decode(
        $this->handleEntityBundle($entity_type_id)->getContent()
      )['total'] ?? NULL;
    }

    return new JsonResponse($data);
  }

  /**
   * Render the bundle endpoint.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The list of data exposed by APY.
   */
  public function handleEntityBundle(string $entity_type_id, string $entity_bundle = NULL) {
    $data = [];

    $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);
    $q = $this->entityTypeManager()->getStorage($entity_type_id)->getQuery();
    if ($entity_bundle) {
      $q->condition($entity_type->getKey('bundle'), $entity_bundle);
    }

    // Access check by default.
    $q->accessCheck(TRUE);

    $count_query = clone $q;
    $fetch_query = clone $q;

    $query = $this->requestStack->getCurrentRequest()->query->all() ?? [];
    $count = $query['count'] ?? 50;
    $fetch_query->pager($count);

    // Allow modules to alter query.
    $this->moduleHandler()->alter('apy_query', $fetch_query, $entity_type, $entity_bundle);

    foreach ($fetch_query->execute() as $id) {
      $data['results'][$id] = Url::fromRoute("apy.$entity_type_id", [], [
        'query' => ['id' => $id],
      ])->setAbsolute()->toString();
    };

    $data['total'] = $count_query->count()->execute();

    return new JsonResponse($data);
  }

  /**
   * Render the entity as "JSON:APY".
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   * @param array $settings
   *   (optional) A list of settings for APY for this entity's bundle.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The normalized data.
   */
  public function handleGet(EntityInterface $entity, array $settings = []) {
    $data = [];

    $selected_plugins = $settings['plugins'] ?? [];
    foreach ($selected_plugins as $plugin_id) {
      $plugin_settings = $settings['options'][$plugin_id] ?? [];
      if ($plugin = $this->pluginManager->createInstance($plugin_id, $plugin_settings)) {
        $data = array_merge($data, $plugin->normalize($entity));
      }
    }

    return new JsonResponse($data);
  }

  /**
   * Checks access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $method = $this->requestStack->getCurrentRequest()->getMethod();
    $op = $method == 'GET' ? 'view' : ($method == 'DELETE' ? 'delete' : 'update');

    $query = $this->requestStack->getCurrentRequest()->query->all();
    $entity_type = $route->getOption('_apy_entity_type_id') ?? $query['entity_type'] ?? NULL;
    $entity_bundle = $route->getOption('_apy_entity_bundle') ?? $query['entity_bundle'] ?? NULL;
    $uuid = $query['id'] ?? NULL;

    if (!empty($uuid) && $entity_type) {
      $id_key = Uuid::isValid($uuid) ? 'uuid' : 'id';
      $definition = $this->entityTypeManager()->getDefinition($entity_type);
      $entities = $this->entityTypeManager()->getStorage($entity_type)->loadByProperties([
        $definition->getKey($id_key) => $uuid
      ]);
      $entity = reset($entities) ?? NULL;
      if (!$entity instanceof EntityInterface) {
        return AccessResult::forbidden($this->t('Entity not found'));
      }

      return $entity->access($op, $account, TRUE);
    }

    // Dynamic permission per entity, per bundle AND per methods.
    $parts = [];
    $parts[] = 'apy';
    $parts[] = $op;
    $parts[] = $entity_type;
    $permission = implode(' ', array_filter($parts));
    $access = AccessResult::allowedIf($account->hasPermission($permission));

    if (!$access->isAllowed() && $entity_bundle) {
      $bundle_parts = $parts;
      $bundle_parts[] = $entity_bundle;
      $bundle_permission = implode(' ', array_filter($bundle_parts));
      return AccessResult::allowedIf($account->hasPermission($bundle_permission));
    }

    if (!$access->isAllowed() && !$entity_bundle) {
      $definition = $this->entityTypeManager()->getDefinition($entity_type);
      if ($bundle_entity_type_id = $definition->getBundleEntityType()) {
        $bundles = $this->entityTypeManager()->getStorage($bundle_entity_type_id)->getQuery()->execute();
        foreach ($bundles as $bundle_id) {
          $bundle_parts = $parts;
          $bundle_parts[] = $bundle_id;
          $bundle_permission = implode(' ', array_filter($bundle_parts));
          $access = AccessResult::allowedIf($account->hasPermission($bundle_permission));
          if ($access->isAllowed()) {
            return $access;
          }
        }
      }
    }

    return $access;
  }
}
