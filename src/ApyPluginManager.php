<?php

namespace Drupal\apy;

use Drupal\apy\ApyPluginManagerInterface;
use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages apy plugins.
 *
 * @see \Drupal\apy\ApyPluginInterface
 * @see \Drupal\apy\Plugin\Apy\ApyPluginBase
 * @see \Drupal\apy\Annotation\Apy
 * @see hook_apy_info_alter()
 */
class ApyPluginManager extends DefaultPluginManager implements ApyPluginManagerInterface, FallbackPluginManagerInterface {

  /**
   * Constructs a new ApyPluginManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler for the alter hook.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Apy', $namespaces, $module_handler, 'Drupal\apy\ApyPluginInterface', 'Drupal\apy\Annotation\Apy');

    $this->alterInfo('apy_info');
    $this->setCacheBackend($cache_backend, 'apy_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'default';
  }
}
