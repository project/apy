<?php

namespace Drupal\apy\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for apy routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * The configuration factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Register admin tab per entity type.
    $entity_types = $this->entityTypeManager->getDefinitions();
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if ($route = $this->getApyEntityRoute($entity_type)) {
        $collection->add("entity.$entity_type_id.apy", $route);
      }
    }

    $config = $this->configFactory->get('apy.settings');
    $endpoint = $config ? $config->get('endpoint') : NULL;

    // Disable APY if endpoint is not valid.
    if (!$endpoint || $endpoint == '/') {
      return;
    }

    // Main endpoint.
    if ($route = $this->getApyEndpointRoute($endpoint)) {
      $collection->add('apy', $route);
    }

    // Entity type endpoint.
    foreach (array_keys($entity_types) as $entity_type_id) {
      if ($route = $this->getApyEntityTypeRoute($endpoint, $entity_type_id)) {
        $collection->add("apy.$entity_type_id", $route);
      }

      // Bundle route.
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      if ($bundle_entity_type_id = $entity_type->getBundleEntityType()) {
        $bundles = $this->entityTypeManager->getStorage($bundle_entity_type_id)->loadMultiple();
        foreach (array_keys($bundles) as $bundle_id) {
          if ($route = $this->getApyEntityBundleRoute($endpoint, $entity_type_id, $bundle_id)) {
            $collection->add("apy.$entity_type_id.$bundle_id", $route);
          }
        }
      }
    }
  }

  /**
   * Gets the route for the local task per entity.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getApyEntityRoute(EntityTypeInterface $entity_type) {
    if ($path = $entity_type->getLinkTemplate('apy')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($path);
      $route
        ->addDefaults([
          '_controller' => 'apy.controller:fromApyRoute',
          '_title_callback' => 'apy.controller::fromApyRouteTitle',
        ])
        ->addRequirements([
          '_custom_access' => 'apy.controller::access'
        ])
        ->setOption('_admin_route', TRUE)
        ->setOption('_apy_entity_type_id', $entity_type_id)
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      return $route;
    }
  }

  /**
   * Gets the base endpoint for APY.
   * 
   * @param string $endpoint
   *   The path for our endpoint.
   * 
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getApyEndpointRoute($endpoint) {
    $route = new Route($endpoint);
    $route
      ->addDefaults([
        '_controller' => 'apy.controller:endpoint',
        '_title_callback' => 'apy.controller::endpointTitle',
      ])
      ->addRequirements([
        '_custom_access' => 'apy.controller::access'
      ])
      ->setOption('_apy', 'TRUE');

    return $route;
  }

  /**
   * Gets the base endpoint for APY.
   * 
   * @param string $endpoint
   *   The path for our endpoint.
   * @param string $entity_type_id
   *   A given entity type ID.
   * 
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getApyEntityTypeRoute($endpoint, $entity_type_id) {
    $route = new Route("$endpoint/$entity_type_id");
    $route
      ->addDefaults([
        '_controller' => 'apy.controller:endpoint',
        '_title_callback' => 'apy.controller::endpointTitle',
      ])
      ->addRequirements([
        '_custom_access' => 'apy.controller::access'
      ])
      ->setOption('_apy', 'TRUE')
      ->setOption('_apy_entity_type_id', $entity_type_id);

    return $route;
  }

  /**
   * Gets the endpoint for a bundle.
   * 
   * @param string $endpoint
   *   The path for our endpoint.
   * @param string $entity_type_id
   *   A given entity type ID.
   * @param string $bundle_id
   *   A given entity bundle ID.
   * 
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getApyEntityBundleRoute($endpoint, $entity_type_id, $bundle_id) {
    $route = new Route("$endpoint/$entity_type_id/$bundle_id");
    $route
      ->addDefaults([
        '_controller' => 'apy.controller:endpoint',
        '_title_callback' => 'apy.controller::endpointTitle',
      ])
      ->addRequirements([
        '_custom_access' => 'apy.controller::access'
      ])
      ->setOption('_apy', 'TRUE')
      ->setOption('_apy_entity_type_id', $entity_type_id)
      ->setOption('_apy_entity_bundle', $bundle_id)
      ->setOption('parameters', [
        $entity_type_id => ['type' => 'entity:' . $entity_type_id],
      ]);

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 100];
    return $events;
  }
}
