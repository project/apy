<?php

namespace Drupal\apy;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an interface for a plugin to format content.
 *
 * Plugins of this type need to be annotated with
 * \Drupal\apy\Annotation\Apy annotation, and placed in the
 * Plugin\Apy namespace directory. They are managed by the
 * \Drupal\apy\ApyPluginManager plugin manager class. There is a base
 * class that may be helpful:
 * \Drupal\apy\Plugin\Apy\ApyPluginBase.
 */
interface ApyPluginInterface extends PluginInspectionInterface {

  /**
   * Run the apy plugin normalizer.
   */
  public function normalize(EntityInterface $entity);
}
