<?php

namespace Drupal\apy;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Manipulates entity type information.
 */
class EntityTypeInfo {

  use StringTranslationTrait;

  /**
   * Adds APY link to all entity types.
   *
   * This is an alter hook bridge.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   The master entity type list to alter.
   *
   * @see hook_entity_type_alter()
   */
  public function entityTypeAlter(array &$entity_types) {
    foreach ($entity_types as $entity_type_id => $entity_type) {
      $entity_type->setLinkTemplate('apy', '/' . $entity_type_id . '/{' . $entity_type_id . '}/apy');
    }
  }

  /**
   * Adds operations on entity that supports it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity on which to define an operation.
   *
   * @return array
   *   An array of operation definitions.
   *
   * @see hook_entity_operation()
   */
  public function entityOperation(EntityInterface $entity) {
    $operations = [];

    if ($entity->hasLinkTemplate('apy') && ($url = $entity->toUrl('apy'))->access()) {
      $operations['apy'] = [
        'title'  => $this->t('APY'),
        'weight' => 100,
        'url'    => $url,
      ];
    }

    return $operations;
  }
}
