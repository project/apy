<?php

namespace Drupal\apy\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an Apy annotation object.
 *
 * @Annotation
 *
 * @see \Drupal\apy\ApyPluginManager
 *
 * @ingroup apy
 */
class Apy extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the apy type.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * An array of default settings.
   *
   * @var array
   */
  public $defaults = [];
  
  /**
   * An array of plugin forms per operation.
   *
   * @var array
   */
  public $forms = [];
}
